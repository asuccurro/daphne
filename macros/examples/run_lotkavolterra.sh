#!/bin/bash
OUTDIR=../../outputs/lotkavolterra/
mkdir -p ${OUTDIR}
conda activate daphnePy2
python lotkavolterra_pureODE.py -p ${OUTDIR}
conda deactivate
