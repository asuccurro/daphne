import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import pandas
from fixed import *

def plotEnjalbert2015_growth1EC(model, expcond, expcond2, path):
    #fig to compare with consortium fig
    n1 = 'EC'
    n2 = 'E. coli'
    bmn1 = 'biomass_%s' % n1
    conctomM = 1000. if model.volumes.externalUnits == 'mL' else 1.
    ybm1 = np.array(model.dmetabolites[bmn1].quantity)
    x = np.array(model.T)
    ygl1 = np.array(model.dmetabolites['ex_glucose'].concentration)*conctomM
    yac1 = np.array(model.dmetabolites['ex_acetate'].concentration)*conctomM

    fig = plt.figure()
    gs = gridspec.GridSpec(2, 1)
    # Top left
    ax1 = fig.add_subplot(gs[0,0])
    ax2 = fig.add_subplot(gs[1,0], sharex=ax1)
    ax1.set_ylabel('Q [gDW]')
    ax2.set_ylabel('C [mM]')
    ax2.set_xlabel('Time [hours]')
    tit = '%s %s' % (n2, expcondLabels[expcond])
    ## COLORS http://htmlcolorcodes.com/color-chart/
    ## blues: #2980b9 #3498db  #1abc9c #16a085
    ## yellows: #f4d03f f5b041 eb984e  #dc7633
    bmn1 = 'biomass_EC'
    tit = tit+', pFBA'

    ax1.plot(x, ybm1, 'k', label=bmn1.replace('_', ' '))
    #ax1.set_xlim(0., 1.)
    ll = ax1.legend(loc='center right', prop={'size':10})
    ax1.set_title(tit)
    ax2.plot(x, ygl1, '#c0392b', linestyle='--', label='Glucose')
    ax2.plot(x, yac1,  '#f39c12', linestyle='--', label='Acetate')

    fignum = ''
    if expcond == "batch_low_Glc":
        # dataAC=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig2A_ac.csv', sep=',',header=None, names=['x','y'])
        # dataBM=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig2A_bm.csv', sep=',',header=None, names=['x','y'])
        # dataGL=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig2A_gl.csv', sep=',',header=None, names=['x','y'])
        # ax1.plot(dataBM['x'], dataBM['y']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        # ax2.plot(dataGL['x'], dataGL['y'], 'rs', label='Glucose (exp)')
        # ax2.plot(dataAC['x'], dataAC['y'], '#f1c40f',  linestyle='None', marker='s', label='Acetate (exp)')
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig2_fromEnjalbert2015.csv', sep=',')
        ax1.errorbar(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, yerr=dataFIG['OD SD']*ODtoGDW*volExt, fmt='bs', label='Biomass (exp)')
        ax2.errorbar(dataFIG['Time'], dataFIG['Glucose mM'], yerr=dataFIG['Glucose SD'], fmt='rs', label='Glucose (exp)')
        ax2.errorbar(dataFIG['Time'], dataFIG['Acetate mM'], yerr=dataFIG['Acetate SD'], label='Acetate (exp)', color='#f1c40f',  linestyle='None', marker='s')
        fignum = '_fig2A'
    elif expcond == "fedbatch_low_Ac":
        # dataBM=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6a_bm.csv', sep=',',header=None, names=['x','y'])
        # ax1.plot(dataBM['x'], dataBM['y']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        #ax1.errorbar(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, yerr=dataFIG['OD SD']*ODtoGDW*volExt, fmt='bs', label='Biomass (exp)')
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6a_fromEnjalbert2015.csv', sep=',')
        ax1.plot(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        fignum = '_fig6A'
    elif expcond == "fedbatch_high_Ac":
        # dataBM=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6c_bm.csv', sep=',',header=None, names=['x','y'])
        # ax1.plot(dataBM['x'], dataBM['y']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6b_fromEnjalbert2015_4h.csv', sep=',')
        ax1.plot(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        fignum = '_fig6C'

    #ax.plot(x, ybm1, ':', label='BM1')
    #ax.plot(x, ybm2, ':', label='BM2')
    #l = ax1.legend(loc='best', prop={'size':10})
    ll = ax1.legend(loc='best', prop={'size':10})
    ll2 = ax2.legend(loc='best', prop={'size':10})
    plt.setp(ax1.get_xticklabels(), visible=False)
    fig.savefig('./enjalbert2015-%s-%s%s.png' % (expcond2, n1, fignum))

    return

def plotEnjalbert2015_growth2EC(model, expcond, expcond2, path):

    n1, n2 = model.dmodelsKeys
    tit = '%s %s %s, pFBA' % (n1, n2, expcondLabels[expcond])
    
    bmn1 = 'biomass_%s' % n1
    bmn2 = 'biomass_%s' % n2

    conctomM = 1000. if model.dmodels[n1].volumes.externalUnits == 'mL' else 1.

    x = np.array(model.dmodels[n1].T)
    x2=np.array(model.dmodels[n2].T)
    ybm1 = np.array(model.dmodels[n1].dmetabolites[bmn1].quantity)
    ygl1 = np.array(model.dmodels[n1].dmetabolites['ex_glucose'].concentration)*conctomM
    yac1 = np.array(model.dmodels[n1].dmetabolites['ex_acetate'].concentration)*conctomM
    #yam1 = np.array(model.dmodels[n1].dmetabolites['ex_ammonium'].quantity)
    ybm2 = np.array(model.dmodels[n2].dmetabolites[bmn2].quantity)
    yac2 = np.array(model.dmodels[n2].dmetabolites['ex_acetate'].concentration)*conctomM
    ygl2 = np.array(model.dmodels[n2].dmetabolites['ex_glucose'].concentration)*conctomM
    #yam2 = np.array(model.dmodels[n2].dmetabolites['ex_ammonium'].quantity)

    fig = plt.figure()
    gs = gridspec.GridSpec(2, 1)
    # Top left
    ax1 = fig.add_subplot(gs[0,0])
    ax2 = fig.add_subplot(gs[1,0], sharex=ax1)
    ax1.set_ylabel('Q [gDW]')
    ax2.set_ylabel('C [mM]')
    ax2.set_xlabel('Time [hours]')

    ## COLORS http://htmlcolorcodes.com/color-chart/
    ## blues: #2980b9 #3498db  #1abc9c #16a085
    ## yellows: #f4d03f f5b041 eb984e  #dc7633 
    ax1.plot(x, ybm1, '#2980b9', label=bmn1.replace('_', ' '))
    ax1.plot(x, ybm2, '#16a085', label=bmn2.replace('_', ' '))
    #ax1.set_xlim(0., 1.)
    ll = ax1.legend(loc='center right', prop={'size':10})
    ax1.set_title(tit)
    ax2.plot(x, ygl1, '#c0392b', label='Glucose')
    ax2.plot(x, yac1, '#f39c12', label='Acetate')
    # ax2.plot(x, ygl1, '#3498db', label='Glucose 1')
    # ax2.plot(x, yac1, '#1abc9c', label='Acetate 1')
    # ax2.plot(x, ygl2, '#f5b041', label='Glucose 2')
    # ax2.plot(x, yac2, '#eb984e', label='Acetate 2')
    ax1.plot(x, ybm1+ybm2, 'k', label='biomass Tot')

    fignum = ''
    if expcond == "batch_low_Glc":
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig2_fromEnjalbert2015.csv', sep=',')
        ax1.errorbar(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, yerr=dataFIG['OD SD']*ODtoGDW*volExt, fmt='bs', label='Biomass (exp)')
        ax2.errorbar(dataFIG['Time'], dataFIG['Glucose mM'], yerr=dataFIG['Glucose SD'], fmt='rs', label='Glucose (exp)')
        ax2.errorbar(dataFIG['Time'], dataFIG['Acetate mM'], yerr=dataFIG['Acetate SD'], label='Acetate (exp)', color='#f1c40f',  linestyle='None', marker='s')
        fignum = '_fig2A'
    elif expcond == "fedbatch_low_Ac":
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6a_fromEnjalbert2015.csv', sep=',')
        ax1.plot(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        fignum = '_fig6A'
    elif expcond == "fedbatch_high_Ac":
        dataFIG=pandas.read_csv(path+'ecoli/enjalbert2015_data/fig6b_fromEnjalbert2015_4h.csv', sep=',')
        ax1.plot(dataFIG['Time'], dataFIG['OD 600nm']*ODtoGDW*volExt, 'bs', label='Biomass (exp)')
        fignum = '_fig6C'
    ll = ax1.legend(loc='best', prop={'size':10})
    ll2 = ax2.legend(loc='best', prop={'size':10})
    plt.setp(ax1.get_xticklabels(), visible=False)
    fig.savefig('./enjalbert2015-%s-totBM%s.png' % (expcond2, fignum))
    return
